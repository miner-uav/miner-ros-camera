#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qnode.h"
#include "cvimagewidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int argc, char** argv, QWidget *parent = 0);
    ~MainWindow();

public Q_SLOTS:
    void batteryVolts(float volts);    
    void newImage(const cv::Mat &img);
private slots:

private:
     QScopedPointer<Ui::MainWindow> ui;
     sampleRosQt::QNode qnode;
     CVImageWidget cvWidget;
};

#endif // MAINWINDOW_H
