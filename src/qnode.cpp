/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <sstream>
#include "qnode.h"
#include "qimage.h"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace sampleRosQt {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :
    init_argc(argc),
    init_argv(argv)
    {}

QNode::~QNode() {
    if(ros::isStarted()) {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
    wait();
}

bool QNode::init() {
    ros::init(init_argc,init_argv,"ros_hdmi_camera");
    if ( ! ros::master::check() ) {
        return false;
    }
    ros::start(); // explicitly needed since our nodehandle is going out of scope.
    ros::NodeHandle n;

    battertSub = n.subscribe("/dji_sdk/battery_state", 1, &QNode::BatteryState, this);
    image_transport::ImageTransport it(n);
    imageSub = it.subscribe("/loitor_stereo_visensor/left/image_raw", 1, &QNode::ImageCbk, this, image_transport::TransportHints("compressed"));
    timer.start();
    start();
    return true;
}


void QNode::run() {
    ros::Rate loop_rate(30);
    int count = 0;
    while ( ros::ok() ) {

        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }
    std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
    Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
}



void QNode::BatteryState(const sensor_msgs::BatteryStateConstPtr& msg){

    Q_EMIT batteryVolts(msg->voltage);
}

void QNode::ImageCbk(const sensor_msgs::ImageConstPtr& msg){

    if(counter % 10 == 0)
    {
        double freq = 10.0 / (timer.restart() /1000.0);
        qInfo() << "ROS message freq:" << freq;

    }
    counter++;
    cv_bridge::CvImageConstPtr image = cv_bridge::toCvShare(msg);
    //const QImage dest((const uchar *) _tmp.data, _tmp.cols, _tmp.rows, QImage::Format_RGB888);

    Q_EMIT newRosImage(image->image);
}

}  // namespace sample-qt-ros
