#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(int argc, char** argv, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    qnode(argc,argv),
    cvWidget(parent)
{
    ui->setupUi(this);
    //this->cvWidget = new CVImageWidget(parent);
    //ui->centralWidget = (QWidget*)&(this->cvWidget);
    ui->verticalLayout->addWidget((QWidget*)&this->cvWidget);

    qnode.init();
    QObject::connect(&qnode, SIGNAL(batteryVolts(float)), this, SLOT(batteryVolts(float)));
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));
    QObject::connect(&qnode, SIGNAL(newRosImage(const cv::Mat&)), this, SLOT(newImage(const cv::Mat&)));
}

MainWindow::~MainWindow()
{

}


void MainWindow::batteryVolts(float volts)
{
    QString str;

    if(volts <= 15.561)
    {
        str.sprintf("Battery LOW %.3f Volts", volts);
        ui->statusBar->showMessage(str);
        QPalette p;
        p.setColor(QPalette::Background, Qt::red);
        ui->statusBar->setAutoFillBackground(true);
        ui->statusBar->setPalette(p);
    }
    else
    {
        str.sprintf("Battery %.3f Volts", volts);
        ui->statusBar->showMessage(str);
        ui->statusBar->setAutoFillBackground(false);
    }
}

void MainWindow::newImage(const cv::Mat &img)
{
    this->cvWidget.showImage(img);
    //QImage dest((const uchar *) cvmat.data, cvmat.cols, cvmat.rows, QImage::Format_RGB888);
    //ui->label->setPixmap(QPixmap::fromImage(img));
}
