#ifndef QNODE_H
#define QNODE_H


/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <QThread>
#include <QStringListModel>
#include <QElapsedTimer>
#include <QDebug>
#include <sensor_msgs/BatteryState.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include "image_transport/image_transport.h"



/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace sampleRosQt {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode : public QThread {
    Q_OBJECT
public:
    QNode(int argc, char** argv );
    virtual ~QNode();
    bool init();
    void run();



    void BatteryState(const sensor_msgs::BatteryStateConstPtr &msg);
    void ImageCbk(const sensor_msgs::ImageConstPtr &msg);
Q_SIGNALS:
    void batteryVolts(float volts);
    void rosShutdown();
    void newRosImage(const cv::Mat &image);

private:
    int init_argc;
    char** init_argv;
    ros::Subscriber battertSub;
    image_transport::Subscriber imageSub;
    QElapsedTimer timer;
    int counter;
};

}  // namespace sample-qt-ros
#endif // QNODE_H
